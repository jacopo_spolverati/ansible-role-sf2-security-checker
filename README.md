Ruolo per automaizzare il check di progetti sf2 installando
https://github.com/sensiolabs/security-checker

In caso di problemi viene aperta una issue su un tracker jira
Nella cartella `test` è presente un playbook di esempio utilizzabile 
tramite `test.sh`


##Playbook Esempio
```
  - name: sf2 security check
    hosts: localhost
    vars_file:
      - vars/main.yml
    roles:
      - { role: ansible-role-sf2-security-checker } 
```


Parametri in `vars.yml`:

```
jenkins_home: "/var/lib/jenkins"
checker_url: "http://get.sensiolabs.org/security-checker.phar"
checker_path: "/usr/local/bin/"
check_bin: "{{checker_path}}/security-checker.phar"

projects_list:

  - name: Bowerphp
    path: jobs/workspace/Bowerphp/composer.lock
    keyid: TEST
    
jira_uri: https://beelab.atlassian.net 
jira_username: ******
jira_password: ****** 
jira_issue_comment: "[~user] "
```
`jenkins_home:` La path della home  di jenkins

`check_bin:` la path del phar per il check

`project_list:` la lista dei progetti da controllare 
  `name:` Nome del progetto
  `path:` path del composer nel workspace di jenkins
  `keyid:` la jey id del progetto su jira






